FROM node
WORKDIR /usr/src/app
COPY app/ /usr/src/app
RUN npm install
CMD "npm" "start"